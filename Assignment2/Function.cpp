#include "Function.h"
#include <cmath>
// set complex number data type
struct Complex
{
	double Real;
	double Imaginary;
	double Component; // flag value for print - 360.1 . 
};
// printing function
void print(Complex Value)
{
	string sign = " + ";
	if (Value.Component != -360.1)
		cout << Value.Component << endl;
	else if (Value.Component == -360.1)
	{
		if (Value.Imaginary < 0)
			sign = " - ";
		cout << Value.Real << sign + " j " << 
abs(Value.Imaginary) << endl;
	}
	else
		cerr << "error in printing value" << endl;

}

 // to compile in g++ takes absolute value of passed double remopve to ompile in visual studio
/*double abs(double value)
{
	if (value < 0)
		return value*-1;
	else
		return value;
}*/
// to compile in g++ converts string to double remove to compile in visual studio
double stof(string str)
{
	istringstream ss(str);
	double value;

	ss >> value;
	if (!ss) 
	{
 	 cerr << "error converting provided value: " << str << endl; 
		return 1;
	}
}

// add complec numbers
Complex add(Complex Variable1, Complex Variable2)
{
	Complex equals;
	equals.Real = Variable1.Real + Variable2.Real;
	equals.Imaginary = Variable1.Imaginary + Variable2.Imaginary;
	return equals;
}
// subtract complex numbers
Complex sub(Complex Variable1, Complex Variable2)
{
	Complex equals;
	equals.Real = Variable1.Real - Variable2.Real;
	equals.Imaginary = Variable1.Imaginary - Variable2.Imaginary;
	return equals;
}
// multiply complex numbers
Complex mul(Complex Variable1, Complex Variable2)
{
	Complex equals;
	equals.Real = Variable1.Real*Variable2.Real - Variable1.Imaginary*Variable2.Imaginary;
	equals.Imaginary = Variable1.Real*Variable2.Imaginary + Variable2.Real*Variable1.Imaginary;
	return equals;
}
// divide complex numbers
Complex div(Complex Variable1, Complex Variable2)
{
	Complex equals;
	double Bottom = Variable2.Real*Variable2.Real + Variable2.Imaginary*Variable2.Imaginary;
	if (Bottom == 0)
	{
		cerr << "Division by 0: input line was d. Dividend = " << Bottom << endl;
		equals.Real = 0;
		equals.Imaginary = 0;
	}

	else
	{
		equals.Real = (Variable1.Real*Variable2.Real + Variable1.Imaginary*Variable2.Imaginary) / Bottom;  // Check this line for sign error
		equals.Imaginary = (-Variable1.Real*Variable2.Imaginary + Variable2.Real*Variable1.Imaginary) / Bottom;
	}
	return equals;
}
//Case statement to select command function. catch outputs error
Complex math(string com, Complex Variable1, Complex Variable2)
{
	Complex result;
	char command = com[0];
	result.Real = 0;
	result.Imaginary = 0;
	if (com.length() > 1)
	{
		result = math_adden(com, Variable1);
	}
	else
	{
		switch (command)
		{
		case 'a':
		case 'A':
			result = add(Variable1, Variable2);
			break;

		case 'm':
		case 'M':
			result = mul(Variable1, Variable2);
			break;

		case 'S':
		case 's':
			result = sub(Variable1, Variable2);
			break;

		case 'D':
		case 'd':
			result = div(Variable1, Variable2);
			break;
		case 'H':
		case 'h':
			help();
			break;
		default:
			cerr << "Malformed command: input line was '" << com << "'" << endl;
		}
	 result.Component = -360.1;
	}
	return result;

}
Complex math_adden(string com, Complex Variable1)
{
	Complex result;
	if (com == "abs" || com == "Abs" || com == "ABS")
		result.Component = absolute(Variable1);
	else if (com == "arg" || com == "Arg" || com == "ARG")
		result.Component = ang(Variable1);
	else if (com == "argDeg" || com == "argdeg" || com == "ArgDeg" || com == "Argdeg" || com == "ARGDEG")
		result.Component = argDeg(Variable1);
	else if (com == "exp" || com == "EXP" || com == "Exp")
		{
		 result = exp(Variable1);
		 result.Component = -360.1;
		}
	else if (com == "inv" || com == "Inv" || com == "INV")
		{
		 result = recip(Variable1);
		 result.Component = -360.1;
		}
	else
	cerr << "Malformed Command: input line was '" << com << "'" << endl;

	return result;
}
double absolute(Complex input)
{
	double output;
	output = sqrt(input.Real * input.Real + input.Imaginary * input.Imaginary);
	return output;

}
double ang(Complex input)
{
	double output;
	if (input.Real == 0)
	{
		cerr << "Division by 0: input line was arg." << endl;
		output = 0;
	}
	else 
		output = atan(input.Imaginary / input.Real);
	return output;
}
double argDeg(Complex input)
{
	double output;
	output = ang(input) * 180 / 3.14159;
	if (input.Real < 0 && input.Imaginary < 0)
		output += -180;
	if (input.Real < 0 && input.Imaginary >= 0)
		output += 180;
	return output;
}
Complex exp(Complex input)
{
	Complex output;
	output.Imaginary = exp(input.Real) * sin(input.Imaginary);
	output.Real = exp(input.Real) * cos(input.Imaginary);
	return output;
}
Complex recip(Complex input)
{
	Complex output, pieces;
	if (input.Real == 0 && input.Imaginary == 0)
	{
		cerr << "Division by 0: input line was inv." << endl;
		return input;
	}
	else
	pieces.Real = 1/absolute(input);
	pieces.Imaginary= -argDeg(input);
	output.Real = pieces.Real * cos(pieces.Imaginary);
	output.Imaginary = pieces.Real * sin(pieces.Imaginary);
	return output;
}
void help(void)
{
	cout << "Welcome to Complex Calculator Version 2.  To add two complex numbers press a"	 << endl;
	cout << "and enter both in the form x y. To subtract two complex numbers press s and "	 << endl;
	cout << "enter both in the form x y. To multiply two complex numbers press a and "		 << endl;
	cout << "enter both in the form x y. To divide two complex numbers press a and enter "	 << endl;
	cout << "both in the form x y. To find the absoulute magnitude of the value type abs"	 << endl;
	cout << " followed by the x y components. To find the angle in degrees type argdeg "	 << endl;
	cout << "followed by the x y components. To find the angle in radians type arg"			 << endl;
	cout << "followed by the x y components.To invert the value type inv followed by the"	 << endl;
	cout << "x y components.To find the exponent equivelent of the number type exp followed" << endl;
	cout << "by the x y components. To quit at anytime press q."							 << endl;

}
