/**
Along with Source.cpp and function.cpp this program is for submission to assignment 1.
Program constructs complex number class and allows basic math operations to be performed
by stating a command and providing to sets of double values. Input expected is a string with 
the format "char double double double double" any other input should generate and error message
that will be displayed.  tested with file Data.txt to check if works with file input. 
*/
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;
struct Complex;

Complex add(Complex, Complex);
Complex sub(Complex, Complex);
Complex div(Complex, Complex);
Complex mul(Complex, Complex);
Complex math(string, Complex, Complex);
Complex math_adden(string, Complex);
double absolute(Complex);
double ang(Complex);
double argDeg(Complex);
Complex exp(Complex);
Complex recip(Complex);

//double abs(double);
double stof(string);
void print(Complex);
void help(void);
