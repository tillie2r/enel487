#include "Function.h"
struct Complex
{
	double Real;
	double Imaginary;
	double Component; // flag for printing function
};
int main()
{
	bool quit = false;
	string input_string;
	Complex number1, number2;
	number1.Component = -360.1;
	string command, token;
	cout << "Enter a command or press h for help. " << endl;
	while (quit == false)
	{
		token = "0";
		getline(std::cin, input_string);

		istringstream to_parse(input_string);
		getline(to_parse, command, ' ');
			// convert string to components
		getline(to_parse, token, ' ');
		number1.Real = stof(token);
		token = "0";			 // reset token
		getline(to_parse, token, ' ');
		number1.Imaginary = stof(token);
		token = "0"; 			// token reset
		getline(to_parse, token, ' ');
		number2.Real = stof(token);
		token = "0";			// token reset
		getline(to_parse, token, ' ');
		number2.Imaginary = stof(token);
		// set loop till command is q
		if (command == "q" || command == "Q")
			quit = true;
		else
			print(math(command, number1, number2));
		

	}

	return 0;
}
