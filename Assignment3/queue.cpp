/**
   Project: Implementation of a Queue in C++.
   Programmer: Karim Naqvi
   Course: enel487
*/

#include "queue.h"
#include <iostream>
#include <cstdlib>              // for exit

using namespace std;

Queue::Queue()
{
    head = 0;
    tail = 0;
    nelements = 0;
    verbose = false;
}

Queue::~Queue()
{
    for (QElement* qe = head; qe != 0;)
    {
	QElement* temp = qe;
	qe = qe->next;
	delete(temp);
    }
}

void Queue::remove(Data* d)
{
    if (size() > 0)
    {
        QElement* qe = head;
        head = head->next;
        nelements--;
        *d = qe->data;
	delete qe;
    }
    else
    {
        cerr << "Error: Queue is empty.\n";
        exit(1);
    }
}

void Queue::insert(Data d)
{
    if (verbose) std::cout << "insert(d)\n";
    QElement* el = new QElement(d);
    if (size() > 0)
    {
        tail->next = el;
    }
    else
    {
        head = el;
    }
    tail = el;
    nelements++;
}
/*************************************************************************
Insert Function for assignment three, takes a data element and a position 
and inserts value into link list ahead of position. done by setting head of 
queue to temp value then running through que to n-1 postion, inserting element
to the head of mini que then reassigning head to temp head value. 
*************************************************************************/
void Queue::insert(Data d, unsigned position)
{
    if (verbose) std::cout << "insert(d)\n";
    QElement* el = new QElement(d);
	QElement* temp_head = new QElement(d);
    if (position > size())
        std::cerr << "insert: range error." << endl;

    if (position == 0)
    {       
            el->next = head;
            head = el;
    }        
    
    else if (position == size())
    {
        tail->next = el; //el -> New_element
        tail = el;
    }
    else if(position < size())
    {
		temp_head->next = head; // assing head to temp value
		for(unsigned i = 0; i < position-1; i++)
				head = head->next; // find position in chain
		el->next = head->next;  // insert element
		head->next = el;
		head = temp_head->next; // reassign start of queue
	}
		delete temp_head;
		nelements++;
}

bool Queue::search(Data otherData)
{
    QElement* insideEl = head;
    for (int i = 0; i < nelements; i++)
    {
        if (insideEl->data.equals(otherData))
            return true;
        insideEl = insideEl->next;
    }
    return false;
}

void Queue::print() const
{
    QElement* qe = head;
    if (size() > 0)
    {
        for (unsigned i = 0; i < size(); i++)
        {
            cout << i << ":(" << qe->data.x << "," << qe->data.y << ") ";
            qe = qe->next;
        }
    }
    cout << "\n";
}

unsigned Queue::size() const
{
    return nelements;
}
