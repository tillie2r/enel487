#pragma once
#include <stdint.h>
typedef uint8_t crc;

#define POLYNOMIAL 0xD8  /* 11011 followed by 0's */
#define WIDTH  (8 * sizeof(crc))
#define TOPBIT (1 << (WIDTH - 1))



crc
crcSlow(uint8_t const message[], int nBytes);

crc
crcFast(uint8_t const message[], int nBytes);

void
crcInit(void);
