
#include <iostream>
#include "CRC.h"

using namespace std;

int main()
{
	int nbit = 5;
	uint8_t const value[10] = "123456789";
	
	cout << "hello HAL" << endl;
	
	crcInit();
	cout << crcSlow(value, nbit) << endl << endl;
	cout << crcFast(value, nbit) << endl << endl;

	return 0;

}