#include <cmath>
#include <iostream>
#include <ctime>
#include <cstdio>

using namespace std;

double table[361][361];
bool negative = false;

void sine(double);
void table_init();
void Print_table();
int lookup_value(double);

int main(void)
{
	clock_t start;
	double long duration;
	double pi = 3.141592;
	
	table_init();
//	Print_table(); print table for viewing
	double input;
	while (1)
	{
	cout << "give me a value" << endl;
	cin >> input;
int i =0;
while(i < 100000)
{
	start = clock();
	sine(input);
	duration = duration + ((clock() - start)/ (double) CLOCKS_PER_SEC);

i++;
}
	cout <<  "look up table time is " << duration << " sec" << endl;
duration = 0;
i = 0; // 36
	while( i < 100000) // to get large enough time
	{
	start = clock();
	sin(input*pi/180);
	duration = duration + ((clock() - start)/ (double) CLOCKS_PER_SEC);
i++;
	}
	cout << "Sin time is " << duration << "sec" << endl;
i = 0;
duration = 0;
	}
return 0;
}

void sine(double user_input)
{
	double holder = abs(user_input);
	char sign;
	if (user_input < -359.5 || user_input > 359.5)  // bounds check
		cout << "value out of bounds" << endl;
	else if (user_input > 180)
		negative = true;
	else if (user_input < 0 && user_input >-180)  // capture sign
		negative = true;
	else if (user_input < 180 && user_input > 0)
		negative = false;
	else if (user_input < -180)
		negative = false;
	
	//bounds control set value into 0 - 180 degree range 
	if (holder >= 180)
		holder = holder - 180;
	if (negative == true)
		sign = '-';
	else
		sign = '+';

	// Round to nearest .5
	double temp = modf(holder, &holder);
	if (temp < 0.3)
		holder = holder;
	else if (temp < 0.7)
		holder = holder + 0.5;
	else if (temp > 0.7)
			holder = holder + 1;

//	cout << "Look up table says: ";
//	cout << sign << table[lookup_value(holder)][1] << endl;
}

void table_init()
{
	double counter = 0;
	double pi = 3.14159265;

	for (int i = 0; i < 361; i++)
	{
		table[i][0] = counter; // initialize table 1 for degree input
		table[i][1] = sin(counter*pi/180); // initialize table 2 for conversion uotput
		counter = counter + 0.5;  // increment precision counter
	}
}

void Print_table()
{
	for (int i = 0; i < 361; i++)
	{
		cout << table[i][0] << " " << table[i][1] << endl;
	}
}

int lookup_value(double search)
{
	int upper, middle, lower;
middle = search * 2; // Because we know the shape of the look up table
/*	upper = 360;
	lower = 0;
	middle = (upper + lower) / 2;
	while (search != table[middle][0])
	{
		if (search == table[middle][0])
			return table[middle][1];
		else if (search < table[middle][0])
			upper = middle;
		else if (search > table[middle][0])
			lower = middle;
		else if (upper == middle || lower == middle)
			return table[middle][1];
		else
			cout << "error" << endl;
		middle = (upper + lower) / 2;
	}
*/
	return middle;
}
