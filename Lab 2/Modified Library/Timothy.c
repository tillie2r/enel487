/******************************************************************************
 * Name:    lab1_lib.c
 * Description: STM32 peripherals initialization and functions
 * Version: V1.00
 * Author: Robert Tillie
 *  		(Stolen from Dave Duguid / Trevor Douglas)
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 *
 *----------------------------------------------------------------------------
 * History:
 *          V1.00 Initial Version
 *****************************************************************************/
	#include "stm32f10x.h"
	#include "Timothy.h"
  
/*
* Name:					void delay()
* Paramaters: 	32 bit delay value, ( a value of 6000 gives approximately 1 mS of delay)
* Description: 	This function creates a delay
* Functions available for public use
* (Stolen from Dave Duguid / Trevor Douglas)
*/
void delay(uint32_t delay)
{
  unsigned i=0;
	unsigned j = 6000 * delay;
	for(i=0; i< j; ++i){}
}

 
