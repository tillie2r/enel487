/******************************************************************************
 * Name:    Timothy.h
 * Description: STM32 timing initialization
 * Version: V1.00
 * Authors: Rob Tillie
 *  		(Stolen from Dave Duguid / Trevor Douglas)
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 *
 *----------------------------------------------------------------------------
 * History:
 *          V1.00 Initial Version
 *****************************************************************************/
#include <stdint.h>


 //Functions available for public use
 //  		(Stolen from Dave Duguid / Trevor Douglas)
 
 void delay(uint32_t delay); // A general purpose countdown timer delay routine

//Functions Edited by me
