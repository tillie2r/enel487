#ifndef REGISTERS_H
#define REGISTERS_H
#include "util.h"



#define PERIPH_BASE           ((uint32_t)0x40000000)
#define AHBPERIPH_BASE        (PERIPH_BASE + 0x20000)
#define RCC_BASE              (AHBPERIPH_BASE + 0x1000)
#define RCC_APB1ENR           (RCC_BASE + 0x1C)
#define RCC_APB2ENR           (RCC_BASE + 0x18)
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x10000)

#define GPIOA_BASE            (APB2PERIPH_BASE + 0x0800) // Also GpioA_crl
#define GPIOA_CRL             (GPIOA_BASE + 0x00)
#define GPIOA_ODR             (GPIOA_BASE + 0x0C)
#define GPIOA_CRH             (GPIOA_BASE + 0x04)
#define GPIOA_BSRR            (GPIOA_BASE  + 0x10)
#define GPIOA_BRR             (GPIOA_BASE  + 0x14)

#define GPIOB_BASE            (APB2PERIPH_BASE + 0x0C00)
#define GPIOB_CRL             (GPIOB_BASE)
#define GPIOB_ODR             (GPIOB_BASE + 0x0C)
#define GPIOB_CRH             (GPIOB_BASE + 0x04)
#define GPIOB_BSRR            (GPIOB_BASE  + 0x10)
#define GPIOB_BRR             (GPIOB_BASE  + 0x14)
#define GPIOB_IDR             (GPIOB_BASE  + 0x8H)

#define USART_BASE						(PERIPH_BASE + 0x4400) //0x4000 4400
#define USART2_SR							(USART_BASE)
#define USART2_DR							(USART_BASE + 0x04)
#define USART2_BRR						(USART_BASE + 0x08)
#define USART2_CR1						(USART_BASE + 0x0C)
#define USART2_CR2						(USART_BASE + 0x10)
#define USART2_CR3						(USART_BASE + 0x14)
#define USART2_GTPR						(USART_BASE + 0x18)



void setupAPB1(void); // set up some  general usart registers
void setupAPB2(void); // sets up registers for io pins (lights)
void setupUSART2(void); //  setup usart specific registers

#endif      
