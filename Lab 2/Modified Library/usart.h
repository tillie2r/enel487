/******************************************************************************
 * Name:   usart.h
 * Description: STM32 usart initialization
 * Version: V1.00
 * Authors: Rob Tillie
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 *
 *----------------------------------------------------------------------------
 * History:
 *          V1.00 Initial Version
 *****************************************************************************/
#include <stdint.h>
#include "stm32f10x.h"
#include "registers.h"

//Registers are defined in register.h and register.c from Blinky.h(lab1)
extern volatile uint32_t * regRCC_APB2ENR;	 
extern volatile uint32_t * regGPIOB_ODR;
extern volatile uint32_t * regGPIOB_CRH;
extern volatile uint32_t * regGPIOB_CRL;
extern volatile uint32_t * regGPIOB_BSRR;
extern volatile uint32_t * regGPIOB_BRR;

extern volatile uint32_t * regRCC_APB1ENR;
extern volatile uint32_t * regGPIOA_CRL;
extern volatile uint32_t * regGPIOA_ODR;
extern volatile uint32_t * regGPIOA_CRH;
extern volatile uint32_t * regGPIOA_BSRR;
extern volatile uint32_t * regGPIOA_BRR; 

extern volatile uint32_t * regUSART2_SR;
extern volatile uint32_t * regUSART2_DR;
extern volatile uint32_t * regUSART2_BRR;
extern volatile uint32_t * regUSART2_CR1;
extern volatile uint32_t * regUSART2_CR2;
extern volatile uint32_t * regUSART2_CR3;
extern volatile uint32_t * regUSART2_GTPR;

void usart2_init(void);
void usart_send(uint8_t);
uint8_t usart_read(void);
void set_led(void);
uint8_t usart_echo(void);
// Chnage to usart 2
