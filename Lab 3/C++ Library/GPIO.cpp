/******************************************************************************
 * Name:    lab1_lib.c
 * Description: STM32 peripherals initialization and functions
* Version: V1.00
 * Author: Robert Tillie
 *  		(Stolen from Dave Duguid / Trevor Douglas)
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 *
 *----------------------------------------------------------------------------
 * History:
 *          V1.00 Initial Version
 *****************************************************************************/
 #include "stm32f10x.h"
 #include "Tim2.h"
 #include "GPIO.h"
 #include "registers.h"
  
// LED Variables

/*  variables used in Blinky  */
	unsigned led_on_mask = 0x0000ff00;	
	unsigned led_off_mask = 0xff000000;

	unsigned LED1_on =0x00000100; 
	unsigned LED2_on =0x00000200; 
	unsigned LED3_on =0x00000400; 
	unsigned LED4_on =0x00000800;
	unsigned LED5_on =0x00001000; 
	unsigned LED6_on =0x00002000; 
	unsigned LED7_on =0x00004000; 
	unsigned LED8_on =0x00008000;	

	unsigned LED1_off =0x01000000; 
	unsigned LED2_off =0x02000000; 
	unsigned LED3_off =0x04000000; 
	unsigned LED4_off =0x08000000;
	unsigned LED5_off =0x10000000; 
	unsigned LED6_off =0x20000000; 
	unsigned LED7_off =0x40000000; 
	unsigned LED8_off =0x80000000;	
	
// SWITCH AND LED I/O FUNCTIONS

/*************************************************																				
* This function will alternate the LEDs on the VLDiscovery board
* Functions available for public use
* (Stolen from Dave Duguid / Trevor Douglas)
**************************************************/
// Configure LEDs pins
 void Config_LED(void)
 {
	 setupAPB2();
	//Need to setup the the LED's led_on_mask the board
  * regRCC_APB2ENR	|=  0x08; // Enable Port B clock
	* regGPIOB_ODR  	&= ~0x0000FF00;// switch led_off_mask LEDs 
	* regGPIOB_CRH	   =  0x33333333; //50MHz  General Purpose output push-pull
 }
 // pre defined LED pattern
void Led_Cylon (void)
{
	unsigned mask;
	mask = LED1_on;
	unsigned i = 0;
	while(i < 8)
	{
		i++;
		L_ONFF(mask);
		delay(40);
		light_off();
		mask = mask << 1;
	}
	while(i > 0)
	{
		i--;
		mask = mask >> 1;
		L_ONFF(mask);
		delay(40);
		light_off();
	}
}
/**********************************************************************************************
By default switches are set to input

Function, IDR register is the register responsible for reading a input value.  & it with the IDR6 has it check the register withthe
 value read on pin 6
 ****/ 
// toggle passed value in BSRR
void L_ONFF(unsigned mask)
{
	* regGPIOB_BSRR = mask;
}
// toggle on LED with BSRR
void L1_on(void)
 {
	 * regGPIOB_BSRR = LED1_on;
 }
void L2_on(void)
 {
	* regGPIOB_BSRR = LED2_on;
 }
void L3_on(void)
	{
	* regGPIOB_BSRR = LED3_on;
	}
void L4_on(void)
	{
	* regGPIOB_BSRR = LED4_on;
	}
void L5_on(void)
	{
	* regGPIOB_BSRR = LED5_on;
	}				
	
void L6_on(void)
	{
	* regGPIOB_BSRR = LED6_on;
	}		
	
void L7_on(void)
	{
	* regGPIOB_BSRR = LED7_on;
	}		
	
void L8_on(void)
	{
	* regGPIOB_BSRR = LED8_on;
	}		
void light_on(void)
 {
 * regGPIOB_BSRR = led_on_mask;
 }
//toggle off LEDs in BSRR
 void L1_off(void)
 {
	* regGPIOB_BSRR = LED1_off;
 }
void L2_off(void)
 {
	* regGPIOB_BSRR = LED2_off;
 }
void L3_off(void)
 {
	* regGPIOB_BSRR = LED3_off;
 }
void L4_off(void)
 {
	* regGPIOB_BSRR = LED4_off;
 }		
void L5_off(void)
 {
	* regGPIOB_BSRR = LED5_off;
 }
void L6_off(void)
 {
	* regGPIOB_BSRR = LED6_off;
 }
void L7_off(void)
 {
	* regGPIOB_BSRR = LED7_off;
 }
void L8_off(void)
 {
	* regGPIOB_BSRR = LED8_off;
 }			 
void light_off(void)
 {
	* regGPIOB_BSRR = led_off_mask;
 }
 
// Query status of registers LEDs in 0xff00 return 1 if on 0 if off
 unsigned Query_LED(unsigned mask)
{
		if ((*regGPIOB_ODR & mask) == mask)
			return 1;
		else
			return 0;
}
 

