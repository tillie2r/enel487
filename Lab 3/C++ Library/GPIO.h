 
 /******************************************************************************
 * Name:    GPIO.h
 * Description: STM32 General purpose In/out put initialization
 * Version: V1.00
 * Authors: Rob Tillie
  *  		(Stolen from Dave Duguid / Trevor Douglas)
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 *
 *----------------------------------------------------------------------------
 * History:
 *          V1.00 Initial Version
 *****************************************************************************/
#include <stdint.h>
#include "util.h"


//Registers are defined in register.h and register.c from Blinky.h(lab1)
extern volatile uint32_t * regRCC_APB2ENR;	 
extern volatile uint32_t * regGPIOB_ODR;
extern volatile uint32_t * regGPIOB_CRH;
extern volatile uint32_t * regGPIOB_BSRR;
extern volatile uint32_t * regGPIOB_BRR;
extern volatile uint32_t * regGPIOB_IDR;

void Led_Cylon(void); // Flash some LEDs in a pattern determined by the state of the USER switch
 
 /***************************************************************************************************/
void Config_LED(void);
void Config_switch(void);
void L_ONFF(unsigned); 

void light_on(void);
void L1_on(void);
void L2_on(void);
void L3_on(void);
void L4_on(void);
void L5_on(void);
void L6_on(void);
void L7_on(void);
void L8_on(void);

void light_off(void);
void L1_off(void);
void L2_off(void);
void L3_off(void);
void L4_off(void);
void L5_off(void);
void L6_off(void);
void L7_off(void);
void L8_off(void);

unsigned Query_LED(unsigned);
