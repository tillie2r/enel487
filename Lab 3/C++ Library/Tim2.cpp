/******************************************************************************
 * Name:    lab1_lib.c
 * Description: STM32 peripherals initialization and functions
 * Version: V1.00
 * Author: Robert Tillie
 *  		(Stolen from Dave Duguid / Trevor Douglas)
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 *
 *----------------------------------------------------------------------------
 * History:
 *          V1.00 Initial Version
 *****************************************************************************/
	#include "stm32f10x.h"
	#include "Tim2.h"
  
// CLOCK AND TIMING FUNCTIONS 

/*
* Name:					void clockInit()
* Paramaters: 	none
* Description: 	This function will initialize the device internal clock to 24 Mhz
*/



void timer_init(void)
{
	setupAPB1();
	setupTIM2();

	*regRCC_APB1ENR |= 0x1; // ENABLE TIMER 2 - may need to swap with arr
	*regTIM2_CR1 =  0x91;	    // Turn on AUTO RELOAD REGISTER AND cen bits direction down
	*regTIM2_ARR = 0xffff; // count down from max value

}
void timer_interrupt_init(void)
{
	*regTIM2_PSC = 1098; // set 1 hz counter
	*regTIM2_DIER |= 0x1; // set trigger interrupt enable and update interrupt enable
	*regNVIC_SET |= 0x10000000;
}	

/*
* Name:					void delay()
* Paramaters: 	32 bit delay value, ( a value of 6000 gives approximately 1 mS of delay)
* Description: 	This function creates a delay
* Functions available for public use
* (Stolen from Dave Duguid / Trevor Douglas)
*/
void delay(uint32_t delay)
{
  unsigned i=0;
	unsigned j = 6000 * delay;
	for(i=0; i< j; ++i){}
}


int16_t timer_start(void)
{
int16_t current = *regTIM2_CNT;
	return current;
}
int16_t timer_stop(int16_t start_time)
{
	int16_t diff;
	diff = start_time - timer_start(); 
	if (diff < 0)
		diff += 0xffff;
	return diff;
}

void timer_shutdown(void)
{	
	*regTIM2_CR1 &= ~0x91;	    // Turn on AUTO RELOAD REGISTER AND cen bits direction down
	*regRCC_APB1ENR &= ~0x1; // ENABLE TIMER 2 - may need to swap with arr
}
 uint8_t Query_Timer(void)
{
	uint8_t on = 0;
	if (*regTIM2_CR1 & 0x1 == 0x1)
		on = 1;
	
	return on;
}
