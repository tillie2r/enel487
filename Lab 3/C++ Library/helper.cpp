#include "helper.h"

/***** 
Helper functions for main to make code cleaner and easier to read
*/
const unsigned ARRAY_LENGTH = 0x08; // gloabal variable, char array length standard
// Command String const
const	char ON[ARRAY_LENGTH]    = "TURNON";
const	char OFF[ARRAY_LENGTH]   = "TURNOFF";
const char HELP[ARRAY_LENGTH]  = "4HELP";
const	char QUERY[ARRAY_LENGTH] = "QUERY";
const char DATE[ARRAY_LENGTH]  = "4DATE";
/*
**  Main execution switch. Reads in two char arrays and executes if else if clause 
*/
void Execute(char token1[ARRAY_LENGTH], char token2[ARRAY_LENGTH])
{
	if (strcmp(token1,ON) == 0x0) // || strcmp(token1, ON) == 0x0D ) // 0x080006B4 - 0x200000B4 is turnon TRUE = 0X46
	/*   This is correct									this occurs from TURNOFF		This is Sparadic */	
	{
		 Turn_on(token2[0]);
	}
	else if (strcmp(token1, OFF) == 0x0 || strcmp(token1, OFF) == 0x0D) // 0x080006AC
	/*  				common but wrong									random change remains const*/		
	{
		Turn_off(token2[0]);
	}
		else if(strcmp(token1, QUERY) == 0x0)
		{
			Print_String("LED status is: \n");
			usart_send(0x0D);
			Query_Status();
		}
		else if(strcmp(token1, DATE) == 0x0)
		{
			Print_Date();
		}
		else if(strcmp(token1, HELP) == 0x0)
		{
		Print_Help();
		}
			
		else if(strcmp(token1, "TIMER") == 0x0)
		{
			if(strcmp(token2, "ON") == 0x0)
				timer_init();
			else if(strcmp(token2, "OFF") == 0x0)
				timer_shutdown();
			else if(strcmp(token2, "TEST1") == 0x0 && Query_Timer() == 1)
				timer_test1();
			else if(strcmp(token2, "TEST2") == 0x0 && Query_Timer() == 1)
				timer_test2();
			else
				Print_String("Invalid TIMER command");
		}
		
		else
		{
			Print_String("Command not valid: ");
			Print_String(token1);
		}
			Print_String("\n");
			usart_send(0x0D);
}	
// Print function for char arrays
void Print_String(char String[16])
{
	// Takes a character array of size 16 and converts it to a printable string. 
	int i = 0;
	while( String[i] != '\0')
	{
		usart_send(String[i]);
		i++;
	}
}

// prints function command blurb for user
void Print_Help(void)
{
	Print_String("Type TURNON # to turn on an LED.\n");
	usart_send(0x0D);
	Print_String("Type TURNON 0 to turn on all LED.\n");
	usart_send(0x0D);
	Print_String("Type TURNOFF # to turn off an LED.\n");
	usart_send(0x0D);
	Print_String("Type TURNOFF 0 to turn off all LED.\n");
	usart_send(0x0D);
	Print_String("Type QUERY to query LED status.\n");
	usart_send(0x0D);
	Print_String("Type 4DATE to query date program was compiled.");
}
// prints opening welcome message
void Print_Welcome(void)
{
	Print_String("Welcome User.\n");
	usart_send(0x0D);
	Print_String("Type 4HELP for assistance, q to quit.\n");
	usart_send(0x0D);
}

// prints status of all register
void Query_Status(void)
{
  if	(Query_LED(0x0100) == 1)
		{
			Print_String("LED 1 status is on \n");
			usart_send(0x0D);
		}
	else
		{
			Print_String("LED 1 status is off \n");
			usart_send(0x0D);
		}
  if	(Query_LED(0x0200) == 1)
		{
			Print_String("LED 2 status is on \n");
			usart_send(0x0D);
		}
	else
		{
			Print_String("LED 2 status is off \n");
			usart_send(0x0D);
		}
  if	(Query_LED(0x0400) == 1)
		{
			Print_String("LED 3 status is on \n");
			usart_send(0x0D);
		}
	else
		{
			Print_String("LED 3 status is off \n");
			usart_send(0x0D);
		}
  if	(Query_LED(0x0800) == 1)
		{
			Print_String("LED 4 status is on \n");
			usart_send(0x0D);
		}
	else
		{
			Print_String("LED 4 status is off \n");
			usart_send(0x0D);
		}
  if	(Query_LED(0x1000) == 1)
		{
			Print_String("LED 5 status is on \n");
			usart_send(0x0D);
		}
	else
		{
			Print_String("LED 5 status is off \n");
			usart_send(0x0D);
		}
  if	(Query_LED(0x2000) == 1)
		{
			Print_String("LED 6 status is on \n");
			usart_send(0x0D);
		}
	else
		{
			Print_String("LED 6 status is off \n");
			usart_send(0x0D);
		}
  if	(Query_LED(0x4000) == 1)
		{
			Print_String("LED 7 status is on \n");
			usart_send(0x0D);
		}
	else
		{
			Print_String("LED 7 status is off \n");
			usart_send(0x0D);
		}
  if	(Query_LED(0x8000) == 1)
		{
			Print_String("LED 8 status is on \n");
			usart_send(0x0D);
		}
	else
		{
			Print_String("LED 8 status is off \n");
			usart_send(0x0D);
		}
	if	(Query_Timer() == 1)
		{
			Print_String("Timer status is on \n");
			usart_send(0x0D);
		}
	else
		{
			Print_String("Timer status is off \n");
			usart_send(0x0D);
		}
}

// prints date of compilation
void Print_Date(void)
{
	Print_String(" Last compiled on "); 	
	Print_String(__DATE__);
	Print_String(" ");
	Print_String(__TIME__);
	Print_String("\n");
	usart_send(0x0D);
}

// TURNON switch statement of LEDs
void Turn_on(char LED)
{
					switch (LED)
				{
		// turn on an led
					case '0':
						light_on();
						break;		
					case '1':
						L1_on();
						break;			
					case '2':
						L2_on();
						break;
					case '3':
						L3_on();
						break;
					case '4':
						L4_on();
						break;
					case '5':
						L5_on();
						break;
					case '6':
						L6_on();
						break;
					case '7':
						L7_on();
						break;				
					case '8':
						L8_on();
						break;
					default:
						Print_String("Not a valid LED in TURNON \n");
						break;
				}
}

// TURNOFF switch statement of LEDs
void Turn_off(char LED)
{
						switch (LED)
				{
		// turn on an led
					case '0':
						light_off();
						break;		
					case '1':
						L1_off();
						break;			
					case '2':
						L2_off();
						break;
					case '3':
						L3_off();
						break;
					case '4':
						L4_off();
						break;
					case '5':
						L5_off();
						break;
					case '6':
						L6_off();
						break;
					case '7':
						L7_off();
						break;				
					case '8':
						L8_off();
						break;
					default:
						Print_String("Not a valid LED in TURNOFF \n");
						break;
				}
}
