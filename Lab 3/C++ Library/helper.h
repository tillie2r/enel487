#include "GPIO.h"
#include "usart.h"
#include "Tim2.h"
#include "timer_impl.h"
#include <string.h>

void Turn_on(char);
void Turn_off(char);
void Print_String(char[]);
void Execute(char token1[0x10], char token2[0x10]);
void Print_Help(void);
void Print_Welcome(void);
void Query_Status(void);
void Print_Date(void);
