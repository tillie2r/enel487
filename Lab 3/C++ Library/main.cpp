/*First Program in C*/
/*Author - Rob Tillie, Jan 2015*/

#include "stm32f10x.h"
#include "helper.h"
#include "usart.h"
#include "Tim2.h"

const unsigned COMMAND_SIZE = 0x10;
const unsigned TOKEN_SIZE = 0x08;

char message;
int main (void) 
	{ 
	char command[COMMAND_SIZE];
	char token1	[TOKEN_SIZE];
	char token2	[TOKEN_SIZE];
	uint8_t i;
	uint8_t j;
	
	Config_LED();
	usart2_init();
	i = 0;
	j = 0;
	Print_Welcome();

	do //While power is on d the stuff in the braces that follow |
	{ 
			Print_String("\\CMD: ");
		do 
		{
			command[i] = usart_echo(); 		// enter value to string
			if (command[i] == 0x7F && i > 0) // backspace clause. decrement counter clear value from string
			{
				command[i] = 0x0;  
				i = i - 1;
			}
			else if(command[i] != 0x7F) // set string to next value
				i = i + 1;
		}while ((command[i-1] != 0x0D) && i < COMMAND_SIZE);// new line clause - commit string  on enter
		
		// parse string
		i = 0;
		
		while (command[i] != 0x20 && command[i] != 0x0D  && i < COMMAND_SIZE) // parsing clause- commit token 1 on space or enter
		{
			token1[i] = command[i];
			i++;
		}
		
		i++;
		do  // create new array without first word
		{
			token2[j] = command[i];
			i++; j++;
		}while((command[i] != 0x0D) && i < COMMAND_SIZE);
		
		// execute command
		Execute(token1, token2); 
		
		// Reset Tokens
		i = 0;
		while(i < 8)
		{
		 token1[i] = 0x0; 
		 token2[i] = 0x0;
			i++;
		}
		i = 0;
		j = 0;  
		

	}while(strcmp(token1, "QUIT") != 0x00); // QUIT clause
	Print_String("Program terminated \n");
	usart_send(0x0D);
}
// Notes remember to take out the void variable send and the void thing statement 
