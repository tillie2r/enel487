#include "registers.h"
#include "util.h"

volatile uint32_t * regRCC_APB2ENR;
volatile uint32_t * regGPIOB_ODR;
volatile uint32_t * regGPIOB_CRH;
volatile uint32_t * regGPIOB_CRL;
volatile uint32_t * regGPIOB_BSRR;
volatile uint32_t * regGPIOB_BRR;
volatile uint32_t * regGPIOB_IDR;

void setupAPB2(void)  //Sets up GPIOB
{
 	regRCC_APB2ENR 	=  (volatile uint32_t *)RCC_APB2ENR;
  regGPIOB_ODR 		=  (volatile uint32_t *)GPIOB_ODR ; 
	regGPIOB_CRL 		=  (volatile uint32_t *)GPIOB_CRL ; 
	regGPIOB_CRH 		=  (volatile uint32_t *)GPIOB_CRH ; 
	regGPIOB_BSRR 	=  (volatile uint32_t *)GPIOB_BSRR ; 
  regGPIOB_BRR 		=  (volatile uint32_t *)GPIOB_BRR ; 
//  regGPIOB_IDR 		=  (volatile uint32_t *)GPIOB_IDR ; 
}
	
volatile uint32_t * regRCC_APB1ENR;
volatile uint32_t * regGPIOA_CRL;
volatile uint32_t * regGPIOA_ODR;
volatile uint32_t * regGPIOA_CRH;
volatile uint32_t * regGPIOA_BSRR;
volatile uint32_t * regGPIOA_BRR; 

void setupAPB1(void) // Sets up GPIOA
{
 regRCC_APB1ENR	= (volatile uint32_t *)RCC_APB1ENR;
 regGPIOA_CRL 	= (volatile uint32_t *)GPIOA_CRL; 
 regGPIOA_ODR 	= (volatile uint32_t *)GPIOA_ODR; 
 regGPIOA_CRH  	= (volatile uint32_t *)GPIOA_CRH; 
 regGPIOA_BSRR 	= (volatile uint32_t *)GPIOA_BSRR; 
 regGPIOA_BRR  	= (volatile uint32_t *)GPIOA_BRR; 
}

volatile uint32_t * regUSART2_SR;
volatile uint32_t * regUSART2_DR;
volatile uint32_t * regUSART2_BRR;
volatile uint32_t * regUSART2_CR1;
volatile uint32_t * regUSART2_CR2;
volatile uint32_t * regUSART2_CR3;
volatile uint32_t * regUSART2_GTPR;

void setupUSART2(void) // Sets up USart
{
 regUSART2_SR    =  (volatile uint32_t *)USART2_SR; 
 regUSART2_DR    =  (volatile uint32_t *)USART2_DR; 
 regUSART2_BRR   =  (volatile uint32_t *)USART2_BRR; 
 regUSART2_CR1   =  (volatile uint32_t *)USART2_CR1; 
 regUSART2_CR2   =  (volatile uint32_t *)USART2_CR2; 
 regUSART2_CR3   =  (volatile uint32_t *)USART2_CR3; 
 regUSART2_GTPR  =  (volatile uint32_t *)USART2_GTPR; 
}
volatile uint32_t * regTIM2_CR1;
volatile uint32_t * regTIM2_CR2;
volatile uint32_t * regTIM2_SMCR;
volatile uint32_t * regTIM2_DIER;
volatile uint32_t * regTIM2_SR;		
volatile uint32_t * regTIM2_EGR;	
volatile uint32_t * regTIM2_CCER;	
volatile uint32_t * regTIM2_CNT;	
volatile uint32_t * regTIM2_PSC;
volatile uint32_t * regTIM2_ARR;
volatile uint32_t * regNVIC_SET;

void setupTIM2(void) // Sets up USart
{
 regTIM2_CR1 	= (volatile uint32_t *)TIM2_CR1;
 regTIM2_CR2 	= (volatile uint32_t *)TIM2_CR2;
 regTIM2_SMCR = (volatile uint32_t *)TIM2_SMCR;
 regTIM2_DIER = (volatile uint32_t *)TIM2_DIER;
 regTIM2_SR 	= (volatile uint32_t *)TIM2_SR;		
 regTIM2_EGR 	= (volatile uint32_t *)TIM2_EGR;	
 regTIM2_CCER = (volatile uint32_t *)TIM2_CCER;	
 regTIM2_CNT 	= (volatile uint32_t *)TIM2_CNT;	
 regTIM2_PSC 	= (volatile uint32_t *)TIM2_PSC;
 regTIM2_ARR 	= (volatile uint32_t *)TIM2_ARR;
 regNVIC_SET  = (volatile uint32_t *)NVIC_SET;
}
