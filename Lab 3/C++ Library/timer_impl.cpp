#include "timer_impl.h"

unsigned interrupt_mask = 0x0100;
int Random_Gen()
{
	int r = rand();
	return r;
}

void timer_test1(void)
{
	volatile int16_t time, test, Timer_time;
	volatile int32_t a,b;
	volatile int64_t c,d;
	struct small bit8a;
	struct small bit8b;
	struct medium bit128a;
	struct medium bit128b;
	struct large bit1024a;
	struct large bit1024b;
	int i;
	const int TEST_NUM = 100;
	
	// Find timer execution overhead
	test = 0;
	String_Print("Timer overhead: ");
	for( i = 0; i < TEST_NUM; i++)
	{
		time = timer_start();
		test = test + timer_stop(time);
	}
		Timer_time = test / TEST_NUM; 
		Print_Timer(Timer_time);
	
	// Start test 1, adding 2 32 bit int
	test = 0;
	String_Print("Test 1 Results: ");
	for( i = 0; i < TEST_NUM; i++)
	{
		a = rand();
		b = rand();
		time = timer_start();
		a = a + b;
		test = test + timer_stop(time);
	}
	test = test / TEST_NUM - Timer_time;
	Print_Timer(test);
	
	// Start test 2, adding 2 64 bit int
	test = 0;
	String_Print("Test 2 Results: ");
	for( i = 0; i < TEST_NUM; i++)
	{
		c = rand();
		d = rand();
		time = timer_start();
		c = c + d;
		test = test + timer_stop(time);
	}
	test = test/TEST_NUM - Timer_time;
	Print_Timer(test);

	// Start test 3, multiply 2 32 bit int
	test = 0;
	String_Print("Test 3 Results: ");
	for( i = 0; i < TEST_NUM; i++)
	{
		a = rand();
		b = rand();
		time = timer_start();
		a = a * b;
		test = test + timer_stop(time);
	}
	test = test/TEST_NUM - Timer_time;
	Print_Timer(test);
	
	// Start test 4, multiple 2 64 bit int
	test = 0;
	String_Print("Test 4 Results: ");
	for( i = 0; i < TEST_NUM; i++)
	{
		c = rand();
		d = rand();
		time = timer_start();
		c = c * d;
		test = test + timer_stop(time);
	}
	test = test/TEST_NUM - Timer_time;
	Print_Timer(test);
	
	// Start test 5, divide 2 32 bit int
	test = 0;
	String_Print("Test 5 Results: ");
	for( i = 0; i < TEST_NUM; i++)
	{
		a = rand();
		while(b == 0) // confirm b isn't 0 before preceding
			b = rand();

		time = timer_start();
		a = a / b;
		test = test + timer_stop(time);
	}
	test = test/TEST_NUM - Timer_time;
	Print_Timer(test);
	
	// Start test 6, divide 2 64 bit int
	test = 0;
	String_Print("Test 6 Results: ");
	for( i = 0; i < TEST_NUM; i++)
	{
		c = rand();
		while(d == 0) // confirm d isn't 0 before preceding
			d = rand();

		time = timer_start();
		c = c / d;
		test = test + timer_stop(time);
	}
	test = test/ TEST_NUM - Timer_time;
	Print_Timer(test);
	
	// Start test 7, copy a 8 bit struct
	test = 0;
	String_Print("Test 7 Results: ");
	for( i = 0; i < TEST_NUM; i++)
	{
		for(int j = 0; j < 8; j++)
			bit8a.a[j] = rand();

		time = timer_start();
		bit8b = bit8a;
		test = test + timer_stop(time);
	}
	test = test/ TEST_NUM - Timer_time;
	Print_Timer(test);
	bit8a.a[1] = bit8b.a[2];
	
	// Start test 8, copy a 128 bit struct
	test = 0;
	String_Print("Test 8 Results: ");
	for( i = 0; i < TEST_NUM; i++)
	{
		for(int j = 0; j < 128; j++)
			bit128a.a[j] = rand();

		time = timer_start();
		bit128b = bit128a;
		test = test + timer_stop(time);
	}
	bit128a.a[0] = bit128b.a[1];		// use bit*b so that optimization doesn't omit it
	test = test/ TEST_NUM - Timer_time;
	Print_Timer(test);
			
	// Start test 9, 
	test = 0;
	String_Print("Test 9 Results: ");
	for( i = 0; i < TEST_NUM; i++)
	{

		for(int j = 0; j < 512; j++)
			bit1024a.a[j] = rand();
		time = timer_start();
		bit1024b = bit1024a;
		test = test + timer_stop(time);
	}
	test = test/TEST_NUM - Timer_time;
	Print_Timer(test);
	bit1024a.a[0] = bit1024b.a[1];
}

void timer_test2(void)
{
	unsigned mask = 0x1;
	unsigned time2 = *regTIM2_DIER;
	unsigned time = *regTIM2_SR;
	
	
	if((*regTIM2_DIER & mask) != mask)
	{
		timer_interrupt_init();
			// UNDERFLOW Event as tigger
		// must use prescaler
		// TIM1||8_RCR can be configured to count the number of underflows before triggering underflow flag - DEC 1098
		String_Print("Interrupt enabled");
	}
	else 
		String_Print("Test2 failed to start"); 
	
/*	while(* regTIM2_SR & 0x1 == 0x1)
		TIM2_IRQHandler();*/
}

// Print function for char arrays
void String_Print(char String[16])
{
	// Takes a character array of size 16 and converts it to a printable string. 
	int i = 0;
	do
	{
		usart_send(String[i]);
		i++;
	}while( String[i] != '\0'); // 
}
// Find a way to print the result time in decimal
void Print_Timer(int16_t time)
{
	const unsigned ARRAY = 6;
	volatile unsigned i;
	volatile int current = 0;
	volatile int next = 0;
	const unsigned mask = 0xF;
	char Timer_array[ARRAY];
	volatile int dummyb;
	volatile unsigned Dumdum;
	
//		sprintf(Timer_array, '%d', tim);
	// print value passed 
	for(i = 0; i < ARRAY; i ++)
	{
		

		Timer_array[(ARRAY-1)-i] = 0x30; // initialize the array to print zero
		Dumdum = mask << (4*i);
		dummyb = time & Dumdum;
		current = dummyb >> (4*i);
		Timer_array[(ARRAY-1)-i] = current + 0x30;
	}
	for ( i = 0; i < ARRAY; i ++) // sets up array as hex values
	{
		if (Timer_array[i] > 0x39)
			Timer_array[i] = Timer_array[i] + 7;
	}
	Timer_array[0] = '0';
	Timer_array[1] = 'x';
	String_Print(Timer_array);
	String_Print("\n");
	usart_send(0x0D);
	
}
void TIM2_IRQHandler() // INTERUPT handler WORKS AS ANTICIPATED
{
//	&= TIM2_SR_UIE
	*regGPIOB_ODR ^= interrupt_mask; // toggle the LED with XOR mask
	if (interrupt_mask < 0xff00)
		interrupt_mask = interrupt_mask << 1;
	if (interrupt_mask > 0xff00)
		interrupt_mask = 0x0100;	
	*regTIM2_SR &= ~0x1;
}


