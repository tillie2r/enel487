/******************************************************************************
 * Name:    Timothy.h
 * Description: STM32 timing initialization
 * Version: V1.00
 * Authors: Rob Tillie
 *  		(Stolen from Dave Duguid / Trevor Douglas)
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 *
 *----------------------------------------------------------------------------
 * History:
 *          V1.00 Initial Version
 *****************************************************************************/
#include <stdint.h>
#include "registers.h"

extern volatile uint32_t * regRCC_APB1ENR;
extern volatile uint32_t * regTIM2_CR1;
extern volatile uint32_t * regTIM2_CR2;
extern volatile uint32_t * regTIM2_SMCR;
extern volatile uint32_t * regTIM2_DIER;
extern volatile uint32_t * regTIM2_SR;		
extern volatile uint32_t * regTIM2_EGR;	
extern volatile uint32_t * regTIM2_CCER;	
extern volatile uint32_t * regTIM2_CNT;	
extern volatile uint32_t * regTIM2_PSC;
extern volatile uint32_t * regTIM2_ARR;
extern volatile uint32_t * regNVIC_SET;


 //Functions available for public use
 //  		(Stolen from Dave Duguid / Trevor Douglas)
 
 void timer_init(void);
 void delay(uint32_t delay); // A general purpose countdown timer delay routine
 int16_t timer_start(void);
 int16_t timer_stop(int16_t start_time);
 void timer_shutdown(void);

//Functions Edited by me
