#include "timer_impl.h"

void timer_init(void)
{
	setupAPB1();
	setupTIM2();

	*regRCC_APB1ENR |= 0x1; // ENABLE TIMER 2 - may need to swap with arr
	*regTIM2_CR1 =  0x91;	    // Turn on AUTO RELOAD REGISTER AND cen bits direction down
	*regTIM2_ARR = 0xffff; // count down from max value

}
	
void timer_shutdown(void)
{	
	*regTIM2_CR1 &= ~0x91;	    // Turn on AUTO RELOAD REGISTER AND cen bits direction down
	*regRCC_APB1ENR &= ~0x1; // ENABLE TIMER 2 - may need to swap with arr
}

int16_t timer_start(void)
{
int16_t current = *regTIM2_CNT;
	return current;
}
int16_t timer_stop(int16_t start_time)
{
	int16_t diff;
	diff = start_time - timer_start(); 
	if (diff < 0)
		diff += 0xffff;
	return diff;
}


// Print function for char arrays
void String_Print(char String[16])
{
	// Takes a character array of size 16 and converts it to a printable string. 
	int i = 0;
	do
	{
		usart_send(String[i]);
		i++;
	}while( String[i] != '\0'); // 
}
// Find a way to print the result time in decimal
void Print_Timer(int16_t time)
{
	const unsigned ARRAY = 6;
	volatile unsigned i;
	volatile int current = 0;
	volatile int next = 0;
	const unsigned mask = 0xF;
	char Timer_array[ARRAY];
	volatile int dummyb;
	volatile unsigned Dumdum;
	
//		sprintf(Timer_array, '%d', tim);
	// print value passed 
	for(i = 0; i < ARRAY; i ++)
	{
		

		Timer_array[(ARRAY-1)-i] = 0x30; // initialize the array to print zero
		Dumdum = mask << (4*i);
		dummyb = time & Dumdum;
		current = dummyb >> (4*i);
		Timer_array[(ARRAY-1)-i] = current + 0x30;
	}
	for ( i = 0; i < ARRAY; i ++) // sets up array as hex values
	{
		if (Timer_array[i] > 0x39)
			Timer_array[i] = Timer_array[i] + 7;
	}
	Timer_array[0] = '0';
	Timer_array[1] = 'x';
	String_Print(Timer_array);
	String_Print("\n");
	usart_send(0x0D);
	
}
void delay(uint32_t delay)
{
  unsigned i=0;
	unsigned j = 6000 * delay;
	for(i=0; i< j; ++i){}
}


