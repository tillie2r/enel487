/*
These are functions calls to satisfiy lab 3 requirements to test Tim2 functionality
*/
#include "Timer.h"
#include <stdio.h>
#include <stdlib.h>
#include "usart.h"

void String_Print(char[]);
void Print_Timer(int16_t);
void delay(void);

struct small
{
	int8_t a[8];
};
struct medium
{
	int8_t a[128];
};
struct large
{
	int8_t a[512];
};
