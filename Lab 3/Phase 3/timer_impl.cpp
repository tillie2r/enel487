#include "timer_impl.h"

unsigned interrupt_mask = 0x0100;

extern volatile uint32_t * regNVIC_SET;

void timer_init(void)
{
	setupAPB1();
	setupTIM2();

	*regRCC_APB1ENR |= 0x1; // ENABLE TIMER 2 - may need to swap with arr
	*regTIM2_CR1 |=  0x91;	    // Turn on AUTO RELOAD REGISTER AND cen bits direction down
	*regTIM2_ARR = 0xffff; // count down from max value
	*regTIM2_PSC = 1098; // set 1 hz counter
	*regNVIC_SET |= 0x10000000;
	*regTIM2_DIER |= 0x1; // set trigger interrupt enable and update interrupt enable
}
	
void delay(uint32_t delay)
{
  unsigned i=0;
	unsigned j = 6000 * delay;
	for(i=0; i< j; ++i){}
}

void phase3_test(void)
{
  Config_LED();
	timer_init();
	//TIM2_IRQHandler();
	while(1){}
}

void TIM2_IRQHandler(void)
{
	*regGPIOB_ODR ^= interrupt_mask; // toggle the LED with XOR mask
	*regTIM2_SR &= ~0x1;
	if (interrupt_mask < 0xff00)
		interrupt_mask = interrupt_mask << 1;
	if (interrupt_mask > 0xff00)
		interrupt_mask = 0x0100;	

}
