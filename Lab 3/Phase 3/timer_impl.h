/*
These are functions calls to satisfiy lab 3 requirements to test Tim2 functionality
*/
#include "Timer.h"
#include <stdio.h>
#include <stdlib.h>
#include "GPIO.h"

void String_Print(char[]);
void Print_Timer(int16_t);
void phase3_test(void);
void delay(void);
void TIM2_IRQHandler(void);
