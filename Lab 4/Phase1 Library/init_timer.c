/**
   Initialize the timer TIM4.
*/
#include "minimal_regs.h"
void stm32_TimerSetup (void) {

    RCC_APB1ENR |= 1u<<2;	// 1: enable clock for TIM4

    TIM4_PSC = 1; //__PSC(__TIMXCLK, __TIM4_PERIOD); //1  set prescaler
    TIM4_ARR = 35999; //__ARR(__TIMXCLK, __TIM4_PERIOD); //35999  set auto-reload (Does nothing?)

    TIM4_CR1 = 0;		// reset command register 1
    TIM4_CR2 = 0;		// reset command register 2

// effect frequency
    TIM4_PSC = 21;  // 7199 (43 sets to 40ms)
    TIM4_ARR = 0xFFFF; // __TIM4_ARR;   //0x270f set auto-reload 
// *****
    TIM4_CCR1  = 0;      //__TIM4_CCR1;  //0
    TIM4_CCR2  = 0;      //__TIM4_CCR2;  //0
	/*
	Timx_ccR3+4 hold a value ccmR2 sets up the mode the  
	*/
    TIM4_CCR3  = 0x07A8; //__TIM4_CCR3;  //0x1388 <- LED 8 (Axxx holds light, test frequency changing) effects duty
    TIM4_CCR4  = 0x1E00; //__TIM4_CCR4;  //0x09c4 <- LED 9 changing value effects time on(Compare counter value to register value) 
    TIM4_CCMR1 = 0;      //__TIM4_CCMR1; //0 
    TIM4_CCMR2 = 0x6060; //__TIM4_CCMR2; //0x6060 <- turns on PWM1 (7 PWM2) <- off while counter < compare value (pin 9- pin 8)
    TIM4_CCER  = 0x1100; //__TIM4_CCER;  //0x1100 set capture/compare enable register
    TIM4_SMCR  = 0;      //__TIM4_SMCR;    // 0 set slave mode control register <- lights turnon on here

    TIM4_CR1 = 1u<<2;    // 1: URS: Only counter overflow/underflow
			  // generates an update interrupt or DMA
			  // request if enabled.

    TIM4_CR2 = 0;        //__TIM4_CR2; // 0x0 set command register 2


    TIM4_CR1 |= 1u<<0;  // 0: enable timer <- starts flickering

} // end of stm32_TimSetup


