/*----------------------------------------------------------------------------
 STM32 GPIO setup.
 initializes the GPIOx_CRL and GPIOxCRH register
 *----------------------------------------------------------------------------*/
#include "minimal_regs.h"

void stm32_GpioSetup (void) {
  
    RCC_APB2ENR |= 1u<<3;	// bit 3: IOPBEN=1, enable GPIOB clock
    
    GPIOB_CRL = 	 // 0 set Port configuration register low
				(0xBu<<28);  // bits 31:28, PB15, output mode, 50MHz, push-pull
    GPIOB_CRH = 
        (3u<<28) | // bits 31:28, PB15, output mode, 50MHz, push-pull
        (3u<<24) | // bits 27:24, PB14, output mode, 50MHz, push-pull
        (3u<<20) | // bits 23:20, PB13, output mode, 50MHz, push-pull
        (3u<<16) | // bits 19:16, PB12, output mode, 50MHz, push-pull
        (3u<<12) | // bits 15:12, PB11, output mode, 50MHz, push-pull
        (3u<< 8) | // bits 11:8,  PB10, output mode, 50MHz, push-pull
/*
Bleow section sets maximum frequencies the clocks can oberate at. B 50 Mhz, A 10, 9 2	
	
*/
        // configure PB9 as TIM4_CH4 output
        (0xBu<<4) | // bits 7:4,  PB9,  output mode, 50MHz, alt func open drain
        // configure PB8 as TIM4_CH3 output
        (0xBu<<0);  // bits 3:0,  PB8,  output mode, 50MHz, alt func open drain

} // end of stm32_GpioSetup

