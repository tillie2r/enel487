/******************************************************************************
 * Name:    A2D.c
 * Description: STM32 peripherals initialization and functions dor anolog inputs
* Version: V1.00
 * Author: Robert Tillie
 *  		(Stolen from Dave Duguid / Trevor Douglas)
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 *
 *----------------------------------------------------------------------------
 * History:
 *          V1.00 Initial Version
 *****************************************************************************/
 #include "stm32f10x.h"
 #include "A2D.h"
 
 // make sure clock is initialized before calling this library.
 
 // Use clock A - clock is turned on once with the use of clock init in timothy.c (on pin 9)
 //pg 173

  void A2DInit(void)
{
	 RCC->APB2ENR |=  RCC_APB2ENR_IOPCEN; // Set C clock 
	 RCC->APB2ENR |= RCC_APB2ENR_ADC1EN; //set clock
	 // Default state at start 0100 			// Set as floating input
	 // GPIOC->CRL &= ~GPIO_CRL_MODE0 & ~GPIO_CRL_CNF0;	 // mask 1011 to crl bit // changed last mask

	ADC1->CR2 |= ADC_CR2_CAL || ADC_CR2_ADON; // turnon and calibrate
	delay(2);
	ADC1-> SMPR1 |= ADC_SMPR1_SMP10_0 || ADC_SMPR1_SMP10_2; //0x5set sample register to use SmP10 @ 55.5cycles pg 235 
	delay(2);
	ADC1-> SQR3 |= ADC_SQR3_SQ1_3 || ADC_SQR3_SQ1_1; // set register 10 as first in sequence
	delay(2);
	}

void A2D_start(unsigned channel)
{
	ADC1-> SQR3 &= ~ADC_SQR3_SQ1; // select channel to check
	ADC1-> SQR3 |= channel; // select channel to check
	delay(2);
	ADC1-> CR1 |= ADC_CR1_SCAN | ADC_CR1_EOCIE;//SCAN MODE and interrupt enable 0x100 + 0x20
	delay(2);
	ADC1-> CR2 |= ADC_CR2_EXTTRIG	| ADC_CR2_SWSTART;//external triggering start conversion of regular channels
	delay(2);	
	ADC1->CR2 |= ADC_CR2_EXTSEL;		// Use SWSTART for external interrupts
	delay (2);
	ADC1-> CR2 |= ADC_CR2_ADON;  // turn on Analog to digital conversion . 
	delay(2);
	
	ADC1-> CR2 |= ADC_CR2_RSTCAL;
	delay(2);
	ADC1 ->CR2 |= ADC_CR2_CAL;
	delay(2);
	ADC1-> CR2 |= ADC_CR2_ADON;
}	
void Interupt_Init(void)
{
	NVIC_EnableIRQ(ADC1_2_IRQn); // Enable global interrupt for adc 1 and 2
}

void delay(uint32_t delay)
{
  unsigned i=0;
	unsigned j = 6000 * delay;
	for(i=0; i< j; ++i)	{}
}
