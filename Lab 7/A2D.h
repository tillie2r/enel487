/******************************************************************************
 * Name:    A2D.h
 * Description: STM32 analog initialization
 * Version: V1.00
 * Authors: Rob Tillie
 *  		(Stolen from Dave Duguid / Trevor Douglas)
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 *
 *----------------------------------------------------------------------------
 * History:
 *          V1.00 Initial Version
 *****************************************************************************/
#include <stdint.h>

 //Functions available for public use
 void A2DInit(void); // Initialize the Cortex M3 analog functions using the RCC registers
 void A2D_start(unsigned);  // set and take value from channel
 unsigned A2D_read(void); // read data from register
 void Interupt_Init(void);
 void delay(uint32_t delay);

