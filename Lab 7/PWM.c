/**
   Initialize the timer TIM4.
*/
#include "PWM.h"
 #include "stm32f10x.h"

const unsigned MINIMUM = 0x07A8; // 700 us
const unsigned MAXIMUM = 0x1800; // 2400 us
//  const unsigned RIG5_OPEN = MINIMUM + (MAXIMUM - MINIMUM)*39/100; // RIG dependent sets mid point rig5
const unsigned RIG5_OPEN = MINIMUM + (MAXIMUM - MINIMUM)*22/25; // RIG dependent sets mid point rig1

void stm32_TimerSetup (void) {

    RCC->APB1ENR |= 1u<<2;	// 1: enable clock for TIM4

// effect frequency
    TIM4->PSC = 2;  // 7199 (43 sets to 40ms)
    TIM4->ARR = 0xD100;// new 20 ms // __TIM4_ARR;   //0x270f set auto-reload 
	// ***** Note: left side back pins third in. PWM signal
		// pwm clock frequency is out
		TIM4->CR1 = 0;		// reset command register 1
    TIM4->CR2 = 0;		// reset command register 2

    TIM4->CCR1  = 0;			  //__TIM4_CCR1;  //0 0x07A8 - 0x1E00
    TIM4->CCR2  = 0x0;      //__TIM4_CCR2;  //0 <- pb7 ***
	/*
	Timx_ccR3+4 hold a value ccmR2 sets up the mode the  
	*/
    TIM4->CCR3  = 0x0; //__TIM4_CCR3;  //0x1388 <- LED 8 (Axxx holds light, test frequency changing) effects duty
    TIM4->CCR4  = 0x0; //__TIM4_CCR4;  //0x09c4 <- LED 9 changing value effects time on(Compare counter value to register value) 
    TIM4->CCMR1 = 0x6060; //__TIM4_CCMR1; //0 
    TIM4->CCMR2 = 0;			 //__TIM4_CCMR2; //0x6060 <- turns on PWM1 (7 PWM2) <- off while counter < compare value (pin 9- pin 8)
    TIM4->CCER  = 0x0011; //__TIM4_CCER;  //0x1100 set capture/compare enable register
    TIM4->SMCR  = 0;      //__TIM4_SMCR;  // 0 set slave mode control register <- lights turnon on here

    TIM4->CR1 = 1u<<2;    // 1: URS: Only counter overflow/underflow
			  // generates an update interrupt or DMA
			  // request if enabled.

    TIM4->CR2 = 0;        //__TIM4_CR2; // 0x0 set command register 2


    TIM4->CR1 |= 1u<<0;  // 0: enable timer <- starts flickering

} // end of stm32_TimSetup


void stm32_GpioSetup (void) {
    RCC->APB2ENR |= 1u<<3;	// bit 3: IOPBEN=1, enable GPIOB clock
    
    GPIOB->CRL = 	 // 0 set Port configuration register low
				(0xBu<<28);  // bits 31:28, PB15, output mode, 50MHz, push-pull
    GPIOB->CRH = 
        (3u<<28) | // bits 31:28, PB15, output mode, 50MHz, push-pull
        (3u<<24) | // bits 27:24, PB14, output mode, 50MHz, push-pull
        (3u<<20) | // bits 23:20, PB13, output mode, 50MHz, push-pull
        (3u<<16) | // bits 19:16, PB12, output mode, 50MHz, push-pull
        (3u<<12) | // bits 15:12, PB11, output mode, 50MHz, push-pull
        (3u<< 8) | // bits 11:8,  PB10, output mode, 50MHz, push-pull
/*
Bleow section sets maximum frequencies the clocks can oberate at. B 50 Mhz, A 10, 9 2	
	
*/
        // configure PB9 as TIM4_CH4 output
        (0xBu<<4) | // bits 7:4,  PB9,  output mode, 50MHz, alt func open drain
        // configure PB8 as TIM4_CH3 output
        (0xBu<<0);  // bits 3:0,  PB8,  output mode, 50MHz, alt func open drain

} // end of stm32_GpioSetup
void PWM_Control(unsigned percent)
{
	unsigned difference;
	difference = (percent * (MAXIMUM - MINIMUM))/100; // change from 0
	
	TIM4->CCR2  = MINIMUM + difference;
}

void PWM_RIG5_Control(unsigned percent)
{
	unsigned difference, r5_min;
	//for rig 1
	r5_min = MINIMUM + (MAXIMUM -MINIMUM)*81/100; 
	difference = percent * (RIG5_OPEN - r5_min)/100;
	
	// for rig 5
	/*r5_min = MINIMUM + 3*(MAXIMUM -MINIMUM)/10; 
	difference = percent * (RIG5_OPEN - r5_min)/100; // change from 0*/
	
	TIM4->CCR2  = r5_min + difference;
}
