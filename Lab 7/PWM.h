#include <stdint.h>

void stm32_GpioSetup (void);
void PWM_RIG5_Control(unsigned percent);
void PWM_Control(unsigned percent);
void stm32_TimerSetup (void);
