#include "Support_Functions.h"

const unsigned ARRAY_LENGTH = 0x08; // gloabal variable, char array length standard

void Print_String(char String[16])
{
	// Takes a character array of size 16 and converts it to a printable string. 
	int i = 0;
	while( String[i] != '\0')
	{
		usart_send(String[i]);
		i++;
	}
}

void Print_Help(void)
{
	Print_String("Type TURNON # to turn on an LED.\n");
	usart_send(0x0D);
	Print_String("Type TURNON 0 to turn on all LED.\n");
	usart_send(0x0D);
	Print_String("Type TURNOFF # to turn off an LED.\n");
	usart_send(0x0D);
	Print_String("Type TURNOFF 0 to turn off all LED.\n");
	usart_send(0x0D);
	Print_String("Type QUERY to query LED status.\n");
	usart_send(0x0D);
	Print_String("Type SHIFT HELP for SHIFT coommands.\n");
	usart_send(0x0D);
	Print_String("Type TIMER HELP for TIMER coommands.\n");
	usart_send(0x0D);
	Print_String("Type 4DATE to query date program was compiled.");

}
void Counter_Help(void)
{
	Print_String("Type TIMER followed by a command from the following list: .\n");
	usart_send(0x0D);
	Print_String("ON - turns on timer.\n");
	usart_send(0x0D);
	Print_String("TEST1 - to initializ and run test 1.\n");
	usart_send(0x0D);
	Print_String("TEST2 - to initializ and run test2. - broken\n");
	usart_send(0x0D);
	Print_String("OFF - deinitialize timer and disable function.\n");
}
void Shift_Help(void)
{
	Print_String("Type SHIFT followed by a command from the following list: .\n");
	usart_send(0x0D);
	Print_String("ON - turns on PWM control.\n");
	usart_send(0x0D);
	Print_String("XXX - enter value (0-100) to shift control by percent.\n");
	usart_send(0x0D);
	Print_String("OFF - deinitialize timer and disable function.\n");
}
void Sensor_Help(void)
{
	Print_String("Type SENSOR followed by a command from the following list: .\n");
	usart_send(0x0D);
	Print_String("ON - turns on sensor.\n");
	usart_send(0x0D);
	Print_String("XXX - enter value (0-100) to shift control by percent.\n");
	usart_send(0x0D);
	Print_String("OFF - deinitialize timer and disable function.\n");
}
// prints opening welcome message
void Print_Welcome(void)
{
	Print_String("Welcome User.\n\r");
	//usart_send(0x0D);
	Print_String("Please enter the height you want the ball(0-60)cm.\n\r");
//	usart_send(0x0D);
}

unsigned Percent_Modifier(unsigned Current_height, unsigned Defined_Height, unsigned PWM)
{
	int a, local_Percent;
	a = 100 - Current_height*100/Defined_Height;
	local_Percent = PWM - a; 
	
	if (local_Percent < 0)
		PWM = 0;
	else if (local_Percent > 100)
		PWM = 100;
	else
		PWM = local_Percent;
	
	return PWM;
}

unsigned Percent_value(char value[])
{
	unsigned number = 0;
	if(value[1] == 0x00)
		number =  (value[0] - 0x30);
	else if(value[2] == 0x00)
		number = (value[0] - 0x30) * 10 + (value[1] - 0x30);
	else if(value[3] == 0x00)
		number = (value[0]-0x30) * 100 + (value[1] - 0x30) * 10 +(value[2] - 0x30);
	else
	{
		Print_String("input error. Autoset to 0 \n");
		usart_send(0x0D);
		number = 0;
	}	
	if(number > 100)
	{
		Print_String("value over 100%. Auto set to 100 \n");
		usart_send(0x0D);
		number = 100;
	}

	return number;
}
unsigned Set_Height(unsigned Height_cm)
{
	unsigned value_return;
	if (Height_cm > 60)
		Height_cm = 60;
		/*
	double b = -0.226 * Height_cm/5;
	double e = exp(b);
	value_return = 4445.2* e; // missing a 1/5 in equation. */

	unsigned x3 = Height_cm*Height_cm*Height_cm/125;
	unsigned x2 = Height_cm*Height_cm/25;
	value_return = -1.6889*x3 + 67.308*x2 - 911.82*Height_cm/5 + 4516;
	
	return value_return;
}
