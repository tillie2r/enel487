#include <math.h>
#include <string.h>
#include "usart.h"
#include "A2D.h"
#include "PWM.h"

void Print_String(char String[16]);
void Print_Help(void);
void Counter_Help(void);
void Shift_Help(void);
void Sensor_Help(void);

// prints opening welcome message
void Print_Welcome(void);
unsigned Percent_Modifier(unsigned Current_height, unsigned Defined_Height, unsigned PWM);
unsigned Percent_value(char value[]);
unsigned Set_Height(unsigned Height_cm);
