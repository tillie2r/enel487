/******************************************************************************
 * Name:    lab1_lib.c
 * Description: STM32 peripherals initialization and functions
 * Version: V1.00
 * Author: Robert Tillie
 *  		(Stolen from Dave Duguid / Trevor Douglas)
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 *
 *----------------------------------------------------------------------------
 * History:
 *          V1.00 Initial Version
 *****************************************************************************/
	#include "usart.h"
	#include "stm32f10x.h"

// Initialize usart2 functionality
	void usart2_init(void) 
{
	unsigned baudR = 9600;
	unsigned frq = 8000000; // clock frequency cahnged from 36MHz to 8Mhz
	
	// set clocks for USART2 GPIOA and AFIO
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN; //set alternate  function clock and io a port clock
	RCC->APB1ENR |=	RCC_APB1ENR_USART2EN; // set clock for usart2 - 0x00020000

	//configure pins to alternate function PA [2-3]
	GPIOA->CRL &= ~0x400;  	// state Changed to 0000 
	GPIOA->CRL |=  0xB00;// set it to the value B (1011) alternate I/o Pin function
	USART2->CR1 |= USART_CR1_UE | USART_CR1_TE  | USART_CR1_RE ;// configuring usart 2 - reisters matched

	//set baud Rate
	USART2->BRR = frq/baudR;// configure to operate at 9600 bps as by chris
	USART2->CR1 |= USART_CR1_UE | USART_CR1_TE  | USART_CR1_RE ;// configuring usart 2
}
/*
	pin pa 2 is the usart tx  - needs to = 0 to transmit bit 
	pin pa 3 is the usart rx
*/

//send character using usart
void usart_send(uint8_t message)
{
	// set tx flag - done by hardware
 while((USART2->SR & USART_SR_TXE)==0); //txe on bit 8
	// move  message to sending register
	USART2->DR = message;
	// wait for message to send
}

// Check usart regist for value
uint8_t usart_read()
{
uint8_t message;
	message = 0;
	unsigned temp = USART2->SR & USART_SR_RXNE;
	while((USART2->SR & USART_SR_RXNE) != 0) // Rxne on bit 5 /!=
	{temp = USART2->SR & USART_SR_RXNE;
		message = USART2->DR;
	}
	return message;
}

// echo back input. 
uint8_t usart_echo(void)
{
	uint8_t message = 0;
	while (message == 0)
	message = usart_read(); // read is working
	usart_send(message);
	return message;
}

