/******************************************************************************
 * Name:   usart.h
 * Description: STM32 usart initialization
 * Version: V1.00
 * Authors: Rob Tillie
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 *
 *----------------------------------------------------------------------------
 * History:
 *          V1.00 Initial Version
 *****************************************************************************/
#include <stdint.h>
#include "stm32f10x.h"
#include "registers.h"
//#include "util.h"

//Registers are defined in register.h and register.c from Blinky.h(lab1)

void usart2_init(void);
void usart_send(uint8_t);
uint8_t usart_read(void);
uint8_t usart_echo(void);
// Chnage to usart 2
